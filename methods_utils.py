# coding=utf-8
import numpy as np

# Creado por: Daniel Cruzado Poveda

'''
solve_equation devolverá una tupla, donde el primer valor es el número de la incógnita resuelta 
(x, y o z) y el segundo valor será el propio valor de la incógnita. En caso de no haber solución,
devuelve None
'''
def solve_equation(matrix):
    ind_term = recover_independent_terms(matrix)
    # El contador me indica si hay más de una incógnita que tenga valor
    count = 0
    # La posición me indica qué incógnita se va a resolver
    position = -1
    for value in range(0,len(matrix)):
        if matrix[value] != 0 and value != len(matrix)-1:
            count += 1
            position = value
    # Si sólo hay una incógnita con valor, entonces puedo dar resultado
    if count == 1:
        return (position, ind_term/matrix[position])

"""
Convierte todos los elementos de un array en arrays, de forma que
se transforme el array en una matriz.
"""
def convert_elements_in_arrays(array):
    result = np.array([[]])
    for val in range(0,len(array)):
        row = np.array([array[val]])
        result = np.vstack([result, row]) if result.size else row
    return result

"""
Dado un np.array (bien sea un matriz o un array), devolverá
como resultado el mismo, quitando los términos independientes.
"""
def eliminate_independent_terms(matrix):
    # Si hay más de una dimensión hablamos de una matriz
    if matrix.ndim > 1:
        return np.delete(matrix, -1, axis=1)
    else:
        return np.delete(matrix, len(matrix)-1)

"""
Dado un np.array (bien sea un matriz o un array), devolverá
como resultado un np.array que contenga los términos independientes.
"""
def recover_independent_terms(matrix):
    independent_terms = np.array([])
    if matrix.ndim > 1:
        num_rows = matrix.shape[0]
        num_cols = matrix.shape[1]
        for row in range(0, num_rows):
            for col in range(0, num_cols):
                if col == num_cols-1:
                    independent_terms = np.append(independent_terms, matrix[row][col])
    else:
        independent_terms = np.array([matrix[len(matrix)-1]])
    
    return independent_terms

"""
Dada una tupla de incógnita y su valor, y dado un array,
sustituye la incógnita en la ecuación, y devuelve el array
resultante. El array de respuesta incluye el término independiente.
"""
def substitute_factor(factor_value,equation):
    factor = factor_value[0]
    value = factor_value[1]
    new_row = np.array([])
    num_var = equation.shape[0]-1
    equation_response = equation
    equation_response[factor] = equation_response[factor] * value
    equation_response[len(equation)-1] -= equation_response[factor]
    equation_response[factor] = 0 
    return equation_response

"""
Dada dos o tres números, devuelve el mínimo común múltiplo estos
"""
def lcm(x, y, z=1):
    greater = max([x, y, z])
    loop = True
    while(loop):
        if((greater % x == 0) and 
            (greater % y == 0) and
            (greater % z == 0)):
            loop = False
            return greater
        greater += 1

"""
Dada la incógnita escogida, la ecuación sustituida, y la ecuación a sustituir en
la posición de la incógnita escogida, devuelve la ecuación resultante de tal sustitución.
"""
def substitute_equation_in_factor(chosen_factor, equation_substituted, equation_substitute):
    num_var = equation_substituted.shape[0]
    substitution_row = np.dot(equation_substitute, equation_substituted[chosen_factor])
    for var in range(0,num_var):
        result_row = np.array([])
        for var in range(0,num_var):
            if var == chosen_factor:
                result_row = np.append(result_row, 0)
            elif var != num_var-1:
                result_row = np.append(result_row, substitution_row[var] + equation_substituted[var])
            else:
                result_row = np.append(result_row, equation_substituted[var] - substitution_row[var])

        return result_row
