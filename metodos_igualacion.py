# coding=utf-8
import numpy as np
from methods_utils import *

# Creado por: Daniel Cruzado Poveda

"""
Dada un pivote (valores 0, 1, o 2 para la primera, segunda o tercera incógnita), la ecuación
escogida como pivote (valores 0, 1 o 2 para cada fila de la matriz) y una matriz (con formato np.array), 
obtenemos la matriz resultante de igualar, con las dos ecuaciones restantes. En caso de tener sólo dos 
ecuaciones (dimensiones 2x2), entonces la función devuelve una tupla con la incógnita obtenida y el valor 
de la incógnita. Las ecuaciones deben contener el término independiente.
"""

def equalise_method(chosen_factor, chosen_row, matrix):

    num_rows = matrix.shape[0]
    num_cols = matrix.shape[1]

    if num_rows == 2:
        sub_row_1 = ([])
        sub_row_2 = ([])

        ind_terms_1 = recover_independent_terms(matrix[0])
        ind_terms_2 = recover_independent_terms(matrix[1])
        for position in range(0, num_cols):
            if position == chosen_factor:
                sub_row_1 = np.append(sub_row_1, 0)
                sub_row_2 = np.append(sub_row_2, 0)
            elif position != num_cols-1:
                transform_val = float(-1 * matrix[0][position] / 
                    matrix[0][chosen_factor])
                sub_row_1 = np.append(sub_row_1, transform_val)

                transform_val = float(-1 * matrix[1][position] / 
                    matrix[1][chosen_factor])
                sub_row_2 = np.append(sub_row_2, transform_val)

        sub_row_1 = np.append(sub_row_1, ind_terms_1 / matrix[0][chosen_factor])
        sub_row_2 = np.append(sub_row_2, ind_terms_2 / matrix[1][chosen_factor])

        result_row = np.subtract(sub_row_1, sub_row_2)
        result_row[len(result_row)-1] = (-1) * result_row[len(result_row)-1]
        # result_ind_terms = recover_independent_terms(result_row)
        # result_row = eliminate_independent_terms(result_row)
        return result_row
    
    elif num_rows == 3:
        # Hacemos una lista por compresión que coja los otros dos valores que no sean
        # la fila escogida
        operating_rows = [index for index in range(0,num_rows) if index != chosen_row]
        
        # Ahora tenemos que transformar las filas de la matriz, de forma que ahora sean
        # un np.array donde la incógnita escogida es 0, los valores de los términos de
        # las otras incognitas quedan invertidos, permitiendo hacer suma directa con el pivote
        # transformado.

        submatrix = matrix

        # Transformación por medio de pivote

        for row in range(0, num_rows):
            for column in range(0, num_cols):
                pivot_factor = submatrix[row][chosen_factor]
                if (column != chosen_factor and column != num_cols-1):
                    submatrix[row][column] = (-1.0) * submatrix[row][column] / pivot_factor
                elif (column == num_cols-1):
                    submatrix[row][column] = submatrix[row][column] / pivot_factor
            submatrix[row][chosen_factor] = 0

        # Ahora restamos las ecuaciones con la ecuación pivote, y devolvemos la matriz resultado
        submatrix[operating_rows[0]] = np.subtract(submatrix[operating_rows[0]], submatrix[chosen_row])
        submatrix[operating_rows[0]][num_cols-1] = (-1) * submatrix[operating_rows[0]][num_cols-1]
        submatrix[operating_rows[1]] = np.subtract(submatrix[operating_rows[1]], submatrix[chosen_row])
        submatrix[operating_rows[1]][num_cols-1] = (-1) * submatrix[operating_rows[1]][num_cols-1]
        submatrix = np.delete(submatrix, chosen_row, 0)

        return submatrix


if __name__ == '__main__':
    equation1 = np.array([2.0, 1.0, -1.0, -3.0])
    equation2 = np.array([3.0, -1.0, 1.0, 3.0])
    equation3 = np.array([5.0, 0.0, 4.0, 12.0])
    step_1 = equalise_method(0,0, np.array([equation1,equation2,equation3]))
    step_2 = equalise_method(1, 1, step_1)
    result = solve_equation(step_2)
    if (result == 3):
        print(result)
