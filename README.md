# Aplicación Web para la creación y corrección de sistemas de ecuaciones  
  
## Creado por
*** Daniel Cruzado Poveda ***  
  
## Acerca del proyecto
Este proyecto es un Trabajo de Fin de Grado propuesto para su presentación en el curso 19/20. Como tal no es  
perfecto, y contiene ciertos errores en el corrector por pasos, como también alguna de las ejecuciones no de  
las ideas no son del todo perfectas. Se ofrece el código como tal, y no me hago responsable del mal uso que  
queráis darle al mismo. Su objetivo y orientación al publicarlo aquí es que sirva como contenido educativo o  
que permita apoyar ciertas ideas, como también comprender ciertos errores cometidos y mejorar sobre ellos.

## Tecnologías  
Las tecnologías principales utilizadas son:  

- Python para el propio desarrollo del servidor (en concreto, usamos el framework Flask)
- HTML, Javascript y CSS para las páginas web (donde HTML sirve para el contenido estático,   
  Javascript trabaja con los métodos de servidor y crea y muestra elementos dinámicos y CSS  
  aporta los complementos visuales  
- MongoDB como sistema gestor de base de datos. Por motivos obvios, para el String de conexión se ha hecho  
  uso de una variable de entorno. Por tanto **el proyecto NO ESTA LISTO PARA USARSE DIRECTAMENTE**. Se explica  
  el cómo configurarlo en la Documentación (Guía de Instalación y Manual de usuario)  

## Documentación  
Adjunto enlaces a la memoria del programa, donde se explica todo el proceso seguido para la creación del proyecto,  
como también el procedimiento a realizar para ponerlo en funcionamiento (manual de instrucciones e instalación).  

[Haz click aquí para ver la documentación](https://drive.google.com/file/d/1Fmbc7FRMHf4Mm45WxUq2HNAJ1iGhnD4Q/view?usp=sharing)
