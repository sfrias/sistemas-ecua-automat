import bson
import json
import os
import unittest

from flask import Flask
from pymongo import MongoClient

# Creado por Daniel Cruzado Poveda

# Obviamente necesitamos la aplicación para probarla
from main import app

class FlaskTest(unittest.TestCase):

    # Debemos obtener un código 200 si accedemos a la 
    # página de registro
    def test_register_redirect(self):
        tester = app.test_client(self)
        response = tester.get('/register', content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Prueba de un registro de un usuario con unos valores
    # válidos. Ojo, estamos usando una BD real, no un mock
    def test_correct_register(self):
        client = app.test_client(self)
        sent_data = {
            'usuario' : 'antonio',
            'nombre_completo' : 'Antonio Toronto Simeón',
            'email' : 'antoniomail@mail.com',
            'rol' : 'alumno',
            'contraseña' : 'jilgueroagudo'
        }
        response = client.post('/create_user', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de creación de un usuario usando datos de
    # uno ya existente (en este caso el apodo). Se espera
    # un error de código 403
    def test_register_invalid_username(self):
        client = app.test_client(self)
        sent_data = {
            'usuario' : 'elalumno',
            'nombre_completo' : 'Antonio Toronto Simeón',
            'email' : 'antoniomail@mail.com',
            'rol' : 'alumno',
            'contraseña' : 'jilgueroagudo'
        }
        response = client.post('/create_user', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 403)

    # Prueba de creación de un usuario usando datos de
    # uno ya existente (en este caso el email). Se espera
    # un error de código 403
    def test_register_invalid_email(self):
        client = app.test_client(self)
        sent_data = {
            'usuario' : 'Antonio',
            'nombre_completo' : 'Antonio Toronto Simeón',
            'email' : 'alumno@mail.com',
            'rol' : 'alumno',
            'contraseña' : 'jilgueroagudo'
        }
        response = client.post('/create_user', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 403)

    # Prueba de creación de un usuario usando sin
    # explicitar datos. Se espera un error de código
    # 400
    def test_register_emptydata(self):
        client = app.test_client(self)
        sent_data = {}
        response = client.post('/create_user', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 400)

if __name__ == "__main__":
    unittest.main()
